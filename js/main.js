
$(document).ready(function () {
  "use strict";

  var window_width = $(window).width(),
  window_height = window.innerHeight,
  header_height = $(".default-header").height(),
  header_height_static = $(".site-header.static").outerHeight(),
  fitscreen = window_height - header_height;


  // $(".fullscreen").css("height", window_height)
  // $(".fitscreen").css("height", fitscreen);

  //-------- Fixed Header Js ----------//
  $(window).on("scroll", function () {
    if ($(window).scrollTop() >= 80) {
      $('.header-area').addClass('header-fixed');
    }
    else {
      $('.header-area').removeClass('header-fixed');
    }
  });


  //------- Active Nice Select --------//
  $('select').niceSelect();

  $('.img-pop-up').magnificPopup({
    type: 'image',
    gallery: {
      enabled: true
    }
  });





  // -------   Mail Send ajax
  $(document).ready(function () {
    var form = $('#myForm'); // contact form
    var submit = $('.submit-btn'); // submit button
    var alert = $('.alert-msg'); // alert div for show alert message

    // form submit event
    form.on('submit', function (e) {
      e.preventDefault(); // prevent default form submit

      $.ajax({
        url: 'mail.php', // form action url
        type: 'POST', // form submit method get/post
        dataType: 'html', // request type html/json/xml
        data: form.serialize(), // serialize form data
        beforeSend: function () {
          alert.fadeOut();
          submit.html('Sending....'); // change submit button text
        },
        success: function (data) {
          alert.html(data).fadeIn(); // fade in response data
          form.trigger('reset'); // reset form
          submit.attr("style", "display: none !important");; // reset submit button text
        },
        error: function (e) {
          console.log(e)
        }
      });
    });
  });




});
